<?php

namespace App\Controller;

use App\Entity\Block;
use App\Entity\User;
use App\Form\PasswordModifyFormType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/profile")
 */

class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="profile_index")
     */
    public function index(): Response
    {
       return $this->render('profile/index.html.twig', [
           'controller_name'=> 'ProfileController',
       ]);
        
    }
    
    
    /**
     * @Route("/women", name="profile_women")
     */
    public function women(): Response
    {
        
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findBy([], ['id' => 'DESC']);
       
        $blockRepository = $this->getDoctrine()->getRepository(Block::class);       
        $blockedPeople = $blockRepository->findAll();

        $userConnected = $this->getUser();

        $usersToDisplay = [];
        foreach ($users as $u)
        {
            if ($userConnected != $u){
                $isBlocked = false;
                foreach ($blockedPeople as $b){
                    if ($b->getBlocked() == $u and $userConnected == $b->getBlocker()) {
                        $isBlocked = true;
                        break;
                    }
                }
                if (!$isBlocked) {
                    array_push($usersToDisplay, $u);
                }
            }
        }
        return $this->render(
            'profile/women.html.twig',
            [
                "users" => $usersToDisplay
            ]
        );
    }
    /**
     * @Route("/man", name="profile_man")
     */
    public function man(): Response
    {
        
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findBy([], ['id' => 'DESC']);
       
        $blockRepository = $this->getDoctrine()->getRepository(Block::class);       
        $blockedPeople = $blockRepository->findAll();

        $userConnected = $this->getUser();

        $usersToDisplay = [];
        foreach ($users as $u)
        {
            if ($userConnected != $u){
                $isBlocked = false;
                foreach ($blockedPeople as $b){
                    if ($b->getBlocked() == $u and $userConnected == $b->getBlocker()) {
                        $isBlocked = true;
                        break;
                    }
                }
                if (!$isBlocked) {
                    array_push($usersToDisplay, $u);
                }
            }
        }
        return $this->render(
            'profile/man.html.twig',
            [
                "users" => $usersToDisplay
            ]
        );
    }
     /**
     * @Route("/view/{username}", name="profile_view")
     */
    public function view(string $username, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(["username" => $username]);

        if (!$user) {
            $this->createNotFoundException('No user found with username ' . $username);
        }

        return $this->render('profile/view.html.twig', ["user" => $user]);

    }

    /**
     * @Route("/modify-password", name="profile_modify_password")
     */
    public function modifyPassword(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->getUser();
        $form = $this->createForm(PasswordModifyFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $form->get('newPassword')->getData())
            );

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('profile_index');
         }
         return $this->render(
             'profile/modifypassword.html.twig',
            [
                "PasswordModifyForm" => $form->createView()
            ]
        );
    }
    
}
